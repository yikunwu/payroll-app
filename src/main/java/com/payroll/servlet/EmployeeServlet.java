package com.payroll.servlet;

import com.payroll.businessLogic.HourlyEmployee;
import com.payroll.dataAccess.EmployeeDao;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Employee Servlet
 */
@WebServlet(name = "EmployeeServlet", urlPatterns = {"employee"}, loadOnStartup = 3)
public class EmployeeServlet extends HttpServlet {
    /**
     * Get Method
     * @param request
     * @param response
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<HourlyEmployee> employees = EmployeeDao.getInstance().getHourlyEmployees();
        request.setAttribute("employees", employees);

        // page rendering
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("employee.jsp");
        requestDispatcher.forward(request, response);
    }
}
